"""
author: 陈慧丽
time: 2021-4-25 14:24
"""
import pytest

if __name__ == '__main__':
    pytest.main(["-s", "-v", "--html=Outputs/reports/pytest.html", "--alluredir=Outputs/allure"])