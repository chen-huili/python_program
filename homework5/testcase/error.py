"""
author: 陈慧丽
time: 2021-4-21 17:10
"""
class Error(Exception):
    """自定义异常类的基类"""
    pass

class NameEmptyError(Error):
    """姓名为空报异常"""

    def __init__(self, username):
        self.username = username

    def __str__(self):
        """返回异常的描述"""
        return "username不能为空"


class PhoneEmptyError(Error):
    """手机号为空引发异常"""

    def __init__(self, phonenum):
        self.phonenum = phonenum

    def __str__(self):
        """返回异常的描述"""
        return "phonenum不能为空"

class PhonelenError(Error):
    """手机号为空引发异常"""

    def __init__(self, phonenum):
        self.phonenum = phonenum

    def __str__(self):
        """返回异常的描述"""
        return "号码长度不正确"

class IllegalPhoneError(Error):
    """手机号非法引发异常"""
    def __init__(self, phonenumber):
        self.phonenum = phonenumber

    def __str__(self):
        """返回异常的描述"""
        return f"手机号{self.phonenumber}不合法"

class AddMemberError(Error):
    """添加成员失败引发异常"""
    def __init__(self, toast):
        self.toast = toast

    def __str__(self):
        return f"添加成员失败,{self.toast}"


class NoSuchElementError(Error):
        """找不到元素抛出异常"""

        def __init__(self):
            pass

        def __str__(self):
            return "未找到元素"