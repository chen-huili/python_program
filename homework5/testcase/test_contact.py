# Author: 陈慧丽
import allure
import pytest
import yaml
from homework5.page.app import App


def get_datas():
    with open(r'E:\Hogwarts\python_program\homework5\datayml\testdata.yaml', encoding='UTF-8') as f:
        data = yaml.safe_load(f)
    return data

class TestContact:
    def setup(self):
        self.app = App()

    def teardowm(self):
        self.app.stop()

    @allure.story("添加成员")
    @pytest.mark.parametrize('username,phonenumber', get_datas()["Add"]["data"], ids=get_datas()["Add"]["ids"])
    def test_addmember(self, username, phonenumber):
        self.app.start()
        self.main = self.app.goto_main()
        self.main.goto_contactList().goto_addmumberPage().addmumberpage().editmumberpage(username,phonenumber)


    @allure.story("删除成员")
    @pytest.mark.parametrize('username,phonenumber', get_datas()["delete"]["data"], ids=get_datas()["delete"]["ids"])
    def test_deletemember(self, username, phonenumber):
        self.app.start()
        self.main = self.app.goto_main()
        self.main.goto_perinfo(username,phonenumber)


