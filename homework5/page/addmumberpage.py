# Author: 陈慧丽
from appium.webdriver.common.mobileby import MobileBy
from homework5.page.BasePage import BasePage
from homework5.testcase.error import NoSuchElementError


class AddMumberPage(BasePage):
    def addmumberpage(self):
        self.driver.implicitly_wait(5)
        # click [手动输入添加]
        try:
            self.find(MobileBy.XPATH, "//*[@text='手动输入添加']").click()
        except:
            raise NoSuchElementError
        else:
            self.log.logger.info("无异常，可进入编辑成员页")
            from homework5.page.editmumberpage import editmumberpage
            return editmumberpage(self.driver)
