# Author: 陈慧丽
from appium.webdriver.common.mobileby import MobileBy

from homework5.page.BasePage import BasePage
from homework5.page.addmumberpage import AddMumberPage
from homework5.testcase.error import NoSuchElementError


class ContacListPage(BasePage):
    #添加成员

    def goto_addmumberPage(self):
        self.driver.implicitly_wait(5)
        try:
            self.log.logger.debug("开始添加成员")
            self.find(MobileBy.XPATH, "//*[@text='添加成员']").click()
        except:
            raise NoSuchElementError
        else:
            self.log.logger.info("无异常，进入添加成员页")
            return AddMumberPage(self.driver)

