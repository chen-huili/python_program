"""
author: 陈慧丽
time: 2021-4-18 0:50
"""
# from appium import webdriver
# desired_caps={}
# desired_caps['platformName']='Android'
# desired_caps['platformVersion']='10'
# desired_caps['deviceName']='7HX5T19925008383'ork'
# # desired_caps['appActivity']='.launch.LaunchSplashActivity'
# # driver=webdriver.Remote('http://localhost:4723/wd/hub',desired_caps)
# desired_caps['appPackage']='com.tencent.wew
# #7HX5T19925008383
import yaml
from appium.webdriver import webdriver
from appium.webdriver.common.mobileby import MobileBy

from homework5.testcase.log import Loggers


class BasePage:

    def __init__(self, driver: webdriver = None):

        self.driver = driver
        self.log = Loggers(level='info')  # 控制所有log的输出等级

    def find(self, by, value, single=True):
        if single:
            return self.driver.find_element(by, value)
        else:
            return self.driver.find_elements(by, value)

    def swipe_find(self, text, fullmatch=True):
        if fullmatch:
            self.log.logger.debug(f"开始滑动查找完全匹配 {text} 的元素")
            return self.find(MobileBy.ANDROID_UIAUTOMATOR,
                             f'new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new '
                             f'UiSelector().text("{text}").instance(0));')
        else:
            self.log.logger.debug(f"开始滑动查找包含 {text} 的元素")
            return self.find(MobileBy.ANDROID_UIAUTOMATOR,
                             f'new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new '
                             f'UiSelector().textContains("{text}").instance(0));')

    def swipe_up(self, text):
        while True:
            self.log.logger.debug("未找到，滑动")
            # width height
            width = self.driver.get_window_size()['width']
            height = self.driver.get_window_size()['height']
            x1 = int(width * 0.5)
            y1 = int(height * 0.9)
            y2 = int(height * 0.1)
            self.driver.swipe(x1, y1, x1, y2, duration=500)
            member_list = self.driver.find_elements(MobileBy.XPATH, f'//*[@text="{text}"]')
            return member_list

#使用yaml查找元素方法
    def run_steps(self, path, function_name):
        with open(path, encoding="utf-8") as f:
            data = yaml.safe_load(f)
        steps = data.get(function_name)
        # 每一个 step 的格式是 {'action': ,'by': , 'value':}
        for step in steps:
            # 如果键的值是 click 的话，代表想要点击元素
            if step.get("action") == "click":

                if step.get("value") == "//*[@text='删除成员']":
                    self.driver.implicitly_wait(5)
                    self.swipe_find(text='删除成员')
                    self.find(step.get("by"), step.get("value")).click()
                elif step.get("value") != "//*[@text='删除成员']":
                    self.find(step.get("by"), step.get("value")).click()

            # 封装了输入框关键字
            else:
                self.log.logger.error("无法定位到元素")


# if __name__ == '__main__':
#     step = BasePage.run_steps(r"E:\Hogwarts\python_program\homework5\datayml\Detailpage.yaml", "goto_perinfo")
#     print(step)
