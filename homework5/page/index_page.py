# Author: 陈慧丽
from appium.webdriver.common.mobileby import MobileBy
from homework5.page.BasePage import BasePage
from homework5.page.contaclist_page import ContacListPage

#进入首页，首页跳转到通讯页
from homework5.testcase.error import PhoneEmptyError


class IndexPage(BasePage):
    #添加成员时的入口
    def goto_contactList(self):
        self.driver.implicitly_wait(5)
        #click通讯录
        self.find(MobileBy.XPATH, "//*[@text='通讯录']").click()
        return ContacListPage(self.driver)

    #删除成员时的入口
    def goto_perinfo(self, username, phonenumber):
        #click通讯录
        self.find(MobileBy.XPATH, "//*[@text='通讯录']").click()
        self.log.logger.info("判断用户名和号码是否为空，为空则抛出异常")
        self.driver.implicitly_wait(10)
        if len(username) == 0:
            self.log.logger.error("用户名不能为空")
        elif len(phonenumber) == 0:
            self.log.logger.error("号码不能为空")
            raise PhoneEmptyError(phonenumber)
        try:
            self.log.logger.debug(f"开始查找{username}")
            self.swipe_find(text=f"{username}").click()
        except:
            self.log.logger.error(f"未找到{username}，抛出异常")
            from homework5.testcase.error import NameEmptyError
            raise NameEmptyError(username)

        else:
            self.run_steps("../datayml/Detailpage.yaml", "goto_perinfo")
            return IndexPage(self.driver)