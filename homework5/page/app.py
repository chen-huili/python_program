# Author: 陈慧丽

from appium import webdriver
from homework5.page.BasePage import BasePage
from homework5.page.index_page import IndexPage


class App(BasePage):
    def start(self):
        Page = "com.tencent.wework"
        Activity = ".launch.LaunchSplashActivity"
        if self.driver is None:
            desired_caps = {}
            desired_caps['platformName'] = 'Android'
            desired_caps['platformVersion'] = '10'
            desired_caps['deviceName'] = '7HX5T19925008383'
            desired_caps['appPackage'] = Page
            desired_caps['appActivity'] = Activity
            # 防止清空缓存-比如登录信息
            desired_caps["noReset"] = "true"
            # 最重要的一步，与server 建立连接
            self.driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)
            self.driver.implicitly_wait(3)
        else:
            self.driver.start_activity(app_package=Page, app_activity=Activity)
            self.driver.implicitly_wait(10)
        return self

    def restart(self):
        self.driver.close_app()
        self.driver.launch_app()

    def stop(self):
        self.driver.close_app()
        self.driver.quit()

    def goto_main(self):
        # 页面的入口方法
        return IndexPage(self.driver)


# if __name__ == '__main__':
#     app = App()
#     app.start()
