# Author: 陈慧丽
from appium.webdriver.common.mobileby import MobileBy
from selenium.common.exceptions import TimeoutException

from homework5.page.BasePage import BasePage
from homework5.testcase.error import AddMemberError, NameEmptyError, PhoneEmptyError, IllegalPhoneError


class editmumberpage(BasePage):

    def editmumberpage(self,username,phonenumber):

            if len(username)==0:
                self.log.logger.error("username不能为空")
                raise NameEmptyError(username)
            elif len(phonenumber) == 0:
                self.log.logger.error("phonenumber不能为空")
                raise PhoneEmptyError(phonenumber)
            # elif phonenumber is not isdigit():
            #     self.log.logger.error("号码包含其他字符")
            #     raise IllegalPhoneError(phonenumber)

            try:
                self.find(MobileBy.XPATH, "//*[contains(@text, '姓名')]/../*[@text='必填']").send_keys(username)
                self.find(MobileBy.XPATH, "//*[contains(@text, '手机')]/..//*[@text='必填']").send_keys(phonenumber)
                self.find(MobileBy.XPATH, "//*[@text='保存']").click()
                self.driver.implicitly_wait(5)
            #WebDriverWait(self.driver, 10).until(lambda x: x.find_element(MobileBy.XPATH, "//*[@text='添加成功']"))

            except TimeoutException:
                toast = self.find(MobileBy.ID, "com.tencent.wework:id/bee").text
                if toast:
                     self.log.logger.info(f"添加失败，{toast}")
                     self.find(MobileBy.ID, 'com.tencent.wework:id/bei').click()
                     raise AddMemberError(toast)

            else:
                    self.log.logger.info(f"username={username},phonenumber={phonenumber},添加成功，返回添加成员页面")
                    from homework5.page.addmumberpage import AddMumberPage
                    return AddMumberPage(self.driver)
